import { Button, Grid, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import HomeIcon from '@mui/icons-material/Home';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import GroupWorkIcon from '@mui/icons-material/GroupWork';
import LiveTvIcon from '@mui/icons-material/LiveTv';
import { useState, useEffect } from "react";


export default function Header() {

    const [scroll, setScroll] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 50);
        });
    }, []);
    return (
        <div className="header-div" style={{ position: "fixed",  width: "100%", zIndex: 10, backgroundColor: `${scroll ? "rgb(0, 0, 0)" : "rgba(0, 0, 0, 0.3)"}`, color: "white" }}>
            <Grid container mx="3%">
                <Grid item md={2} xs={6} textAlign="center">
                    <Typography component={Link} to="/" variant="h4" textAlign="center" style={{ textDecoration: "none", color: "white", display: `${scroll ? "none" : "block"}` }} fontWeight={900}>KT FILM</Typography>
                </Grid>
                <Grid item md={10} style={{ margin: "auto 0" }} >
                    <Grid container  >
                        <Grid item md={12} xs={12}>
                            <Grid container  >
                                <Grid item md={2} xs={3} textAlign="center" >
                                    <Button  component={Link} style={{ marginBottom: "5px", textDecoration: "none", color: "white", marginRight: "1rem" }} to="/">
                                        <Grid container >
                                            <Grid item md={6} xs={12}><HomeIcon></HomeIcon></Grid>
                                            <Grid item md={6} xs={12}>HOME</Grid>
                                        </Grid>
                                    </Button >
                                </Grid>
                                <Grid item md={2} xs={3} textAlign="center">
                                    <Button  component={Link} style={{ textDecoration: "none", color: "white", marginRight: "1rem" }} to="/trending">
                                        <Grid container >
                                        <Grid item md={6} xs={12}><WhatshotIcon></WhatshotIcon> </Grid>
                                        <Grid item md={6} xs={12}> TRENDING </Grid>
                                        </Grid>

                                    </Button>
                                </Grid>
                                <Grid item md={2} xs={3} textAlign="center">
                                    <Button  component={Link} style={{ textDecoration: "none", color: "white", marginRight: "1rem" }} to="/movies">
                                        <Grid container >
                                        <Grid item md={6} xs={12}> <GroupWorkIcon></GroupWorkIcon> </Grid>
                                        <Grid item md={6} xs={12}> MOVIES </Grid>
                                        </Grid>

                                    </Button>

                                </Grid>
                                <Grid item md={2} xs={3} textAlign="center">
                                    <Button  component={Link} style={{ textDecoration: "none", color: "white", marginRight: "1rem" }} to="/tvseries">
                                        <Grid container >
                                        <Grid item md={6} xs={12}><LiveTvIcon></LiveTvIcon> </Grid>
                                        <Grid item md={6} xs={12}> TVSERIES </Grid>
                                        </Grid>

                                    </Button>
                                </Grid>
                                <Grid item md={4} xs={12} className="btn-login" textAlign="center">
                                    <Button
                                        variant="contained"
                                        style={{
                                            color: "black", backgroundColor: "white",
                                            borderRadius: "45px", fontWeight: 500, fontSize: 12,
                                            height: "25px", width: "130px", marginTop: "2px",
                                            display: `${scroll ? "none" : ""}`
                                        }}
                                    >
                                        Login
                                    </Button>
                                </Grid>
                            </Grid>

                        </Grid>

                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}
