import { Box, Button, Grid, Input } from "@mui/material";
import bgFooter from "../assets/image/footer-bg.jpg"


export default function Footer() {
    return (
        <Grid container style={{ color: "white", backgroundImage:`url(${bgFooter})`,marginTop:"30px" }}>
            <Grid item md={3.5} xs={12} >
                <Box className="footer-box">
                    <h3>KT MOVIES</h3>
                    <div>Cinemy Movies and TV Series</div>
                    <div>Hồ Chí Minh, Thủ Đức</div>
                    <div>Contact: ngthanhnhan.vn@gmail.com</div>
                </Box>
            </Grid>
            <Grid item md={2.5} xs={6}>
                <Box className="footer-box">
                    <h4>Resources</h4>
                    <div>About CinemyPlex</div>
                    <div>Contact Us</div>
                    <div>Forum</div>
                    <div>Blog</div>
                    <div>Help Center</div>
                </Box>
            </Grid>
            <Grid item md={2} xs={6}>
                <Box className="footer-box">
                    <h4>Legal</h4>
                    <div>Terms of Use</div>
                    <div>Privacy Policy</div>
                    <div>Security</div>
                </Box>
            </Grid>
            <Grid item md={4} xs={12}>
                <Box className="footer-box">
                    <h4>Newsletter</h4>
                    <div>Subscribe to our newsletter system now to get latest news from us</div>
                    <Input
                        fullWidth
                        style={{ backgroundColor: "hsla(0,0%,100%,.16)", color: "#fff", width: "95%", height: "40px" }}
                        placeholder="Enter your email"></Input>
                    <Button style={{ color: "red" }} >Subscribe Now</Button>
                </Box>
            </Grid>
        </Grid>
    )
}