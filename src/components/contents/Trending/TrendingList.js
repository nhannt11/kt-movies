import { Grid, Pagination, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { Link, useParams,useNavigate } from "react-router-dom"
import noInfoImg from "../../../assets/image/noInfo.jpeg"




export default function TrendingList() {
    const navigate = useNavigate()
    const params = useParams()
    const[currentPage,setCurrentPage] = useState(params.page!==undefined?parseInt(params.page):1)
    const [trendingList, setTrendingList] = useState([])
    const getData = async (url, body) => {
        const res = await fetch(url, body);
        const resData = await res.json();
        return resData
    }

    useEffect(() => {
        //console.log(params.page)
        
        getData(`https://api.themoviedb.org/3/trending/all/week?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US&page=${params.page !== undefined ? params.page : 1}`)
            .then((res) => {
                //console.log(res.results);
                setTrendingList(res.results)
            })
    }, [params])

    return (
        <div className="top-div"  >
            <Grid container>
                <Typography variant="h5" ml={3} mb={3} color="white">TRENING SHOWS:</Typography>
            </Grid>
            <Grid container justifyContent="center" mb={3}>
            <Pagination onChange={(event,value)=>{navigate(`/trending/page/${value}`);setCurrentPage(value)}} page={currentPage} variant="outlined" count={3}  />  

            </Grid>
            <Grid container justifyContent="center">
                {trendingList.length > 0 ?
                    trendingList.map((el, index) =>
                        <Grid key={index}  item md={1.7} xs={6} mb={3}>
                            <Link to={`/${el.media_type}/${el.id}`} className="single-content">
                                <span style={{
                                    color: el.vote_average > 8 ? "#34cc34" : el.vote_average > 6 ? "orange" : "red"
                                }}>{Math.round(el.vote_average * 10) / 10}</span>
                                <img alt="poster" src={el.poster_path!==null?`https://image.tmdb.org/t/p/w300${el.poster_path}`:noInfoImg} ></img>
                                <div className="content-title">{`${el.media_type!=="tv"?el.title+"("+el.release_date.slice(0, 4)+")":el.name+"("+el.first_air_date.slice(0, 4)+")"}`}</div>
                            </Link>
                        </Grid>

                    )
                    : null}
            </Grid>
        
            <Grid container justifyContent="center">
            <Pagination onChange={(event,value)=>{navigate(`/trending/page/${value}`);setCurrentPage(value)}} page={currentPage} variant="outlined" count={3}  />  

            </Grid>
        </div>
    )
}