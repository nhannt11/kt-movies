import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import YouTubeIcon from '@mui/icons-material/YouTube';
import { Modal, Box, Grid } from "@mui/material";
import ModalVideo from "react-modal-video";
import bgImage from "../../../assets/image/bgImage.png"
import noInfoImg from "../../../assets/image/noInfo.jpeg"




const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
export default function MediaDetail() {
    const params = useParams()
    const [open, setOpen] = useState(false)
    const [movie, setMovies] = useState("")
    const [videos, setVideos] = useState("")
    const getData = async (url, body) => {
        const response = await fetch(url, body)
        const resData = await response.json()
        return resData
    }
    useEffect(() => {
        getData(`https://api.themoviedb.org/3/${params.type}/${params.movieId}?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US`)
            .then(res => {
                //console.log(res)
                setMovies(res)
            })
        getData(`https://api.themoviedb.org/3/${params.type}/${params.movieId}/videos?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US`)
            .then(res => {
                //console.log(res)
                setVideos(res.results)
            })
    }, [params.movieId,params.type])
    const handleClickTrailer = () => {
        //console.log("click")
        setOpen(true)
    }
    return (
        movie !== "" ?
            <Grid  style={{ position: "relative" }}>
                <img alt="poster" className="background-info" style={{
                    display: "block",
                    width: "100%",
                    objectFit: "cover",
                    filter: "brightness(40%)"
                }}
                    src={movie.backdrop_path !== null ? `https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/${movie.backdrop_path}` : bgImage}>
                </img>
                <Grid container className="top-div" style={{ width: "80%", margin: "0 0 0 10%", display: "flex", position: "absolute", top:"40px", color: "white", fontWeight: "600" }} >
                    <Grid item md={4} xs={12} position="relative">
                        <img alt="poster" className="info-img" src={movie.poster_path !== null ? `https://image.tmdb.org/t/p/w300${movie.poster_path}` : noInfoImg} ></img>
                    </Grid>
                    <Grid item md={8} xs={12} style={{ display: "flex", flexDirection: "column", width: "100%", fontSize: "0.95rem", fontWeight: 700 }}>
                        <Grid container>
                        <h3 style={{ color: "white" }}>{params.type === "movie" ? movie.title : movie.name}</h3>

                        </Grid>
                        <Grid container >
                            {params.type !== "tv" ? movie.release_date.slice(0, 4) : movie.first_air_date.slice(0, 4)}. {params.type.toUpperCase()}.&nbsp;
                            <span style={{ color: "#2fc9f3" }}> TMDB</span>
                            - ⭐{Math.round(movie.vote_average * 10) / 10}
                        </Grid>
                        <Grid>
                            {movie.genres.map((el, index) => {
                                return index === 0 ? el.name : " , " + el.name
                            })}
                        </Grid>
                        <Grid container display="block" width="60%"  onClick={handleClickTrailer} className="btn-trailer btn-trailer-success ">
                            <span style={{ color: "red" }}><YouTubeIcon></YouTubeIcon></span>
                            &nbsp;Watch Trailer
                        </Grid>
                        <Grid container style={{ color: "#b3b3b3", fontSize: "1.5rem", marginBottom: "3px" }}>
                            {movie.tagline}
                        </Grid>
                        <Grid style={{ marginBottom: "5px" }}>
                            {movie.overview}
                        </Grid>
                        <Grid style={{ marginBottom: "5px" }}>
                            <span style={{ color: "#b3b3b3" }}>DURATION :</span>
                            {params.type === "movie" ? movie.runtime + " mins" : movie.episode_run_time[0] + " mins per episode"}
                        </Grid>
                        {params.type === "tv" ? <Grid style={{ marginBottom: "5px" }}>
                            <span style={{ color: "#b3b3b3" }}>SEASONS :</span>
                            {movie.number_of_seasons}&nbsp;
                        </Grid> : null}
                        <Grid style={{ marginBottom: "5px" }}>
                            <span style={{ color: "#b3b3b3" }}>STUDIO :</span>
                            {movie.production_companies.map((el, index) => {
                                return index === 0 ? el.name : " , " + el.name

                            })}
                        </Grid>
                        {params.type === "movie" ? <Grid style={{ marginBottom: "5px" }}>
                            <span style={{ color: "#b3b3b3" }}>RELEASE DATE :</span>
                            {movie.release_date}&nbsp;
                        </Grid> : null}

                        <Grid>
                            <span style={{ color: "#b3b3b3" }}>STATUS :</span>
                            {movie.status}
                        </Grid>
                    </Grid>
                </Grid>
                <div>
                    {videos !== "" && videos.length>0 ?
                        <ModalVideo channel='youtube' isOpen={open} videoId={
                            videos.filter(el => el.name === "Official Trailer").length > 0 ?
                                videos.filter(el => el.name === "Official Trailer")[0].key :
                                videos.filter(el => el.type === "Trailer").length > 0 ?
                                    videos.filter(el => el.type === "Trailer")[0].key :
                                    videos[0].key
                        } onClose={() => setOpen(false)} />
                        : <Modal open={open} onClose={() => setOpen(false)}>
                            <Box sx={style}>
                                <img alt="poster" src={noInfoImg}></img>

                            </Box>
                        </Modal>}

                </div>



            </Grid> : null

    )
}