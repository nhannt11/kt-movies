import { Grid } from "@mui/material"
import { useState, useEffect } from "react"
import { Link, useParams } from "react-router-dom"
import noInfoImg from "../../../assets/image/noInfo.jpeg"



export default function RelatedMovies() {
    const params = useParams()
    const [relatedMovies, setRelatedMovies] = useState([])
    const getData = async (url, body) => {
        const res = await fetch(url, body);
        const resData = await res.json();
        return resData
    }
    useEffect(() => {
        getData(`https://api.themoviedb.org/3/${params.type}/${params.movieId}/similar?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US&page=1`)
            .then((res) => {
                //console.log(res.results.slice(0,7));
                setRelatedMovies(res.results.slice(0, 7))
            })
    }, [params.movieId,params.type])
    return (
        <>
        <h4 style={{color:"white" , margin:"30px"}}>You May Also Like</h4>
         <Grid container justifyContent="center" >
            {relatedMovies.length > 0 ?
                relatedMovies.map((el, index) =>
                <Grid key={index} item md={1.7} xs={6} mb={3}>
                    <Link to={`/${params.type}/${el.id}`} className="single-content"  >
                        <span style={{
                            color: el.vote_average > 8 ? "#34cc34" : el.vote_average > 6 ? "orange" : "red"
                        }}>{ Math.round(el.vote_average * 10) / 10}</span>
                        <img alt="poster" src={el.poster_path!=null?`https://image.tmdb.org/t/p/w300${el.poster_path}`:noInfoImg} ></img>
                        <div className="content-title">{`${params.type!=="tv"?el.title+"("+el.release_date.slice(0, 4)+")":el.name+"("+el.first_air_date.slice(0, 4)+")"}`}</div>
                    </Link>
                </Grid>
                    
                )
                : null}
        </Grid>
        </>
       
    )
}