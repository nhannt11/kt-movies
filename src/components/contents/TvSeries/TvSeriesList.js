import { Button, Grid, Input, Pagination, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { Link, useParams, useNavigate } from "react-router-dom"
import { Dropdown, DropdownMenu, DropdownToggle } from "reactstrap"
import noInfoImg from "../../../assets/image/noInfo.jpeg"




export default function TvSeriesList() {
    const navigate = useNavigate()
    const [searchName, setSearchName] = useState("")
    const [selectGenres, setSelectGenres] = useState({ name: "On Air", id: "" })
    const [genresTV, setGenresTV] = useState("")
    const [pages, setPages] = useState(100)
    const params = useParams()
    const [dropdownOpen, setDropdownOpen] = useState(false)
    const [currentPage, setCurrentPage] = useState(params.page !== undefined ? parseInt(params.page) : 1)
    const [tvSeriesList, setTvSeriesList] = useState([])

    const getData = async (url, body) => {
        const res = await fetch(url, body);
        const resData = await res.json();
        return resData
    }
    const toggle = () => setDropdownOpen(!dropdownOpen);
    const handleSearch = () => {
        navigate(`/tvseries/page/1`);
        setSelectGenres({ name: "Searching By Name", id: -1 })
    }
    useEffect(() => {
        //console.log(currentPage + typeof (currentPage))
        setCurrentPage(params.page !== undefined ? parseInt(params.page) : 1)
        //console.log(params.page)
        if (selectGenres.id === "") {
            getData(`https://api.themoviedb.org/3/tv/on_the_air?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US&page=${params.page !== undefined ? params.page : 1}`)
                .then((res) => {
                    //console.log(res);
                    setPages(res.total_pages)
                    setTvSeriesList(res.results)
                })
        }
        else if (selectGenres.id === -1) {
            getData(`https://api.themoviedb.org/3/search/tv?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US&page=${params.page !== undefined ? params.page : 1}&query=${searchName}`)
                .then((res) => {
                   // console.log(res)
                    setPages(res.total_pages)
                    setTvSeriesList(res.results)
                })
        }
        else {
            getData(`https://api.themoviedb.org/3/discover/tv?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US&first_air_date.lte=2022&with_genres=${selectGenres.id}&page=${params.page !== undefined ? params.page : 1}`)
                .then((res) => {
                    //console.log(res);
                    setPages(res.total_pages > 100 ? 100 : res.total_pages)
                    setTvSeriesList(res.results)
                })
        }
        window.scrollTo(0, 0)
    }, [params,searchName,selectGenres.id])
    useEffect(() => {
        
        if (selectGenres.id !== -1) {
            setSearchName("")
        }
    }, [selectGenres])
    useEffect(() => {
        getData("https://api.themoviedb.org/3/genre/tv/list?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US")
            .then((res) => {
                //console.log(res.genres)
                setGenresTV(res.genres)
            })
    }, [])
    return (
        <div className="top-div"  >
            <Grid container direction="row"  >
                <Grid item md={7} xs={12}>
                    <Typography variant="h4" ml={4} mb={3} color="white">TV SERIES:</Typography>
                </Grid>
                <Grid item md={5} xs={12} >
                    <Grid container >
                        <Grid item md={9} xs={9}>
                            <Input
                                value={searchName}
                                onChange={(e) => setSearchName(e.target.value)}
                                style={{ backgroundColor: "hsla(0,0%,100%,.16)", color: "#fff", width: "95%", height: "40px" }}
                                placeholder="  Search Movies Here ...">
                            </Input>
                        </Grid>
                        <Grid item md={3} xs={3}>
                            <Button
                                onClick={handleSearch}
                                variant="contained"
                                style={{marginBottom:"10px" , fontWeight: "600", color: "white", backgroundImage: "linear-gradient(45deg,#00aeff,#a68eff)" }}>
                                Search
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Dropdown isOpen={dropdownOpen} toggle={toggle} style={{ marginBottom: "30px" }} >
                <DropdownToggle style={{ backgroundImage: "linear-gradient(45deg,#00aeff,#a68eff)", marginLeft: "30px", color: "white", fontWeight: "700" }} caret>{selectGenres.name}</DropdownToggle>
                <DropdownMenu style={{ backgroundColor: "#2c2d50!important", width: "95%" }}>
                    <div style={{ display: "flex", flexWrap: "wrap", padding: "15px" }}>
                        <Button style={{ margin: "10px 10px" }} color="secondary" variant="contained" onClick={() => { setSelectGenres({ name: "On Air", id: "" }); toggle();navigate(`/tvseries/page/1`) }} size="small" >On Air</Button>
                        {genresTV !== "" ?
                            genresTV.map((el,index) => <Button key={index} onClick={() => { setSelectGenres({ name: el.name, id: el.id }); toggle();navigate(`/tvseries/page/1`); }} style={{ margin: "10px 10px" }} color="secondary" variant="contained" size="small" >{el.name}</Button>)
                            : ""}
                    </div>
                </DropdownMenu>
            </Dropdown>
            <Grid container justifyContent="center">
                {tvSeriesList.length > 0 ?
                    tvSeriesList.map((el, index) =>
                        <Grid key={index} item md={1.7} xs={6} mb={3}>
                            <Link to={`/tv/${el.id}`} className="single-content"  >
                                <span style={{
                                    color: el.vote_average > 8 ? "#34cc34" : el.vote_average > 6 ? "orange" : "red"
                                }}>{el.vote_average > 0 ? Math.round(el.vote_average * 10) / 10 : "null"}</span>
                                <img alt="thumb" src={el.poster_path !== null ? `https://image.tmdb.org/t/p/w300${el.poster_path}` : noInfoImg} ></img>
                                <div className="content-title">{el.name + `(${el.first_air_date ? el.first_air_date.slice(0, 4) : "unknow"})`}</div>
                            </Link>
                        </Grid>
                    )
                    : null}
            </Grid>
            <Grid container justifyContent="center">
                <Pagination onChange={(event, value) => { navigate(`/tvseries/page/${value}`); setCurrentPage(value) }} page={currentPage} variant="outlined" count={pages} />
            </Grid>
        </div>
    )
}