import { Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import "../../../App.css"
import { Link } from "react-router-dom";

export default function TopRated() {
    const [topRated, setTopRated] = useState([])
    const getData = async (url, body) => {
        const res = await fetch(url, body);
        const resData = await res.json();
        return resData
    }
    useEffect(() => {
        getData("https://api.themoviedb.org/3/movie/top_rated?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US&page=1")
            .then((res) => {
                //console.log(res.results.slice(0,7));
                setTopRated(res.results.slice(0, 7))
            })
    }, [])
    return (
        <div>
            <Typography my={4} ml={5} color="white" variant="h5">TOP RATED</Typography>
            <div  >
                <Grid container justifyContent="center" >
                    {topRated.length > 0 ?
                        topRated.map((el, index) =>
                            <Grid key={index} item md={1.7} xs={6} mb={3}>
                                <Link to={`/movie/${el.id}`} className="single-content"  >
                                    <span style={{
                                        color: el.vote_average > 8 ? "#34cc34" : el.vote_average > 6 ? "orange" : "red"
                                    }}>{Math.round(el.vote_average * 10) / 10}</span>
                                    <img alt="poster" src={`https://image.tmdb.org/t/p/w300${el.poster_path}`} ></img>
                                    <div className="content-title">{el.title + `(${el.release_date.slice(0, 4)})`}</div>
                                </Link>
                            </Grid>
                        )
                        : null}
                </Grid>

            </div>
        </div>
    )
}