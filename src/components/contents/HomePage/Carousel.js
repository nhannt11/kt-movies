import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { Button, Grid } from "@mui/material";
import { useState, useEffect } from "react";

import { Link } from "react-router-dom";


//import image1 from "https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces//yaze6df9AMIA9oeDEbIZ4zOTRCJ.jpg"
const settings = {
    dots: false,
    lazyLoad: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 2,
    arrows : false
};


export default function Carousel() {
    const [trending, setTrending] = useState([])
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    useEffect(() => {
        getData("https://api.themoviedb.org/3/trending/all/day?api_key=39f5897aa2b8f37692fc06e61504587d")
            .then((res) => {
                //console.log(res.results)
                setTrending(res.results)
            })
            .catch((error) => {
                console.log(error)
            })
    }, []
    )
    return (
        <Slider {...settings}>
            {trending.length > 0 ?
                trending.map((movie, index) =>
                    <Grid key={index} container position="relative" >
                        <Grid container >
                        <img alt="backdrop" style={{
                            display: "block", 
                            objectFit:"cover",
                            width:"100%",
                            minHeight:"650px",
                            filter: "brightness(80%)"
                        }}
                            src={`https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/${movie.backdrop_path}`}>
                        </img>
                        </Grid>
                        <Grid item md={6} xs={12} mx="auto"
                        p={3}
                        position="absolute"
                        bottom="0"
                        width="100%" >
                            <h3 style={{color:"white", fontWeight: 700}}>{movie.original_title?movie.original_title:movie.original_name}</h3>
                            <h5 style={{color:"white",fontWeight: 700}}>{movie.media_type.toUpperCase()}</h5>
                            <p className="overview"  style={{color:"white",fontWeight: 700, width:"100%"}} >{movie.overview}</p>
                            <Button component={Link} to={`/${movie.media_type}/${movie.id}`} sx={{backgroundColor:"#661fb8", color:"white", marginBottom:"2rem"}}>Learn more</Button>
                        </Grid>
                    </Grid>


                )

                : null}
        </Slider>
    )
}