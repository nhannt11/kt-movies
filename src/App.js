
import { Routes, Route,BrowserRouter } from 'react-router-dom';
import './App.css';
import Homepage from './pages/Homepage';
import Login from './pages/Login';
import Movies from './pages/Movies';
import Trending from './pages/Trending';
import TvSeries from './pages/TvSeries';
import 'bootstrap/dist/css/bootstrap.min.css';
import MediaDetail from './pages/MediaDetail';
import "react-modal-video/scss/modal-video.scss"



function App() {
  return (
<BrowserRouter>
      <Routes>
        <Route path="/" element={<Homepage></Homepage>}></Route>
        <Route path="/login" element={<Login></Login>}></Route>
        <Route path="/trending" element={<Trending></Trending>}></Route>
        <Route path="/trending/page/:page" element={<Trending></Trending>}></Route>
        <Route path="/movies" element={<Movies></Movies>}></Route>
        <Route path="/movies/page/:page" element={<Movies></Movies>}></Route>
        <Route path="/tvseries" element={<TvSeries></TvSeries>}></Route>
        <Route path="/tvseries/page/:page" element={<TvSeries></TvSeries>}></Route>
        <Route path="/:type/:movieId" element={<MediaDetail></MediaDetail>}></Route>
      </Routes>
    </BrowserRouter> 
    

  );
}

export default App;
