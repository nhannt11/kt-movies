import Carousel from "../components/contents/HomePage/Carousel";
import Header from "../components/Header";

import Movies from "../components/contents/HomePage/Movies";
import TvSeries from "../components/contents/HomePage/TvSeries";
import TopRated from "../components/contents/HomePage/TopRated";

import Footer from "../components/Footer";


export default function Homepage (){
    
    return(
        <div >
        <Header></Header>
        <Carousel></Carousel>
        <Movies></Movies>
        <TvSeries></TvSeries>
        <TopRated></TopRated>
        <Footer></Footer>
        </div >
    )
}