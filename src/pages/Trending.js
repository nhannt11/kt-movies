import TrendingList from "../components/contents/Trending/TrendingList";
import Footer from "../components/Footer";
import Header from "../components/Header";


export default function Trending (){
    return(
        <div style={{backgroundColor:"#180b3b"}}>
        <Header></Header>
        <TrendingList></TrendingList>
        <Footer></Footer>
        </div>
    )
}