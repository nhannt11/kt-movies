import TvSeriesList from "../components/contents/TvSeries/TvSeriesList";
import Footer from "../components/Footer";
import Header from "../components/Header";

export default function TvSeries (){
    return(
        <div style={{backgroundColor:"#180b3b"}}>
        <Header></Header>
        <TvSeriesList></TvSeriesList>
        <Footer></Footer>
        </div>
    )
}