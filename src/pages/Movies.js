import Header from "../components/Header";
import MovieList from "../components/contents/Movies/MovieList";
import Footer from "../components/Footer";

export default function Movies (){
    return(
        <div style={{backgroundColor:"#180b3b"}}>
        <Header></Header>
        <MovieList></MovieList>
        <Footer></Footer>
        </div>
    )
}