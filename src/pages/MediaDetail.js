import { useEffect } from "react"
import Cast from "../components/contents/MediaDetail/Cast"
import MediaDetail from "../components/contents/MediaDetail/MediaDetail"
import RelatedMovies from "../components/contents/MediaDetail/RelatedMovies"
import Header from "../components/Header"
import { useParams } from "react-router-dom"
import Footer from "../components/Footer"




export default function Homepage (){
    const params = useParams()
   useEffect(()=>{
    //console.log("render lai detail")
    window.scrollTo(0,0)
   },[params.movieId])
    return(
        <div >
        <Header></Header>
        <MediaDetail></MediaDetail>
        <Cast></Cast>
        <RelatedMovies></RelatedMovies>
        <Footer></Footer>
        </div >
    )
}